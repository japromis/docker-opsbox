DOCKER_IMAGE=opsbox

include Makefile.docker

PACKAGE_VERSION=0.1

include Makefile.package

.PHONY: check-version
check-version:
	docker run --rm --entrypoint helm $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):$(VERSION) version  --template='{{.Version}}'
	docker run --rm --entrypoint kubectl $(DOCKER_NAMESPACE)/$(DOCKER_IMAGE):$(VERSION) version --short 2>/dev/null | grep 'Client Version:'| awk '{print $$3}'
